var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);


var transaction_table = [];
var support_table = [];
var beacon_count = 0;
var transaction_count = 0;
var valid_transaction_count = 0;
var nodes = [];

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

app.get('/js/:js', function(req, res){
	console.log('/js/'+req.params.js);
	res.sendFile(__dirname + '/js/' +req.params.js);
})

app.get('/js/lib/:js', function(req, res){
	res.sendFile(__dirname + '/js/lib/' + req.params.js);
})

app.get('/style/:style', function(req, res){
	console.log('/style/'+req.params.js);
	res.sendFile(__dirname + '/style/' +req.params.style);
})

app.get('/maps/:map', function(req, res){
	res.sendFile(__dirname + '/maps/' + req.params.map);
})

io.on('connection', function(socket){
	console.log('a user connected');
	socket.on('disconnect', function(){
		console.log('user disconnected');
	});
	
	socket.on('upload', function(file){
		var result = analyzeAssociate(file);
		var max = 0;
		for(var i=0; i<result.length; i++){
			for(var j=0; j<result[i].length; j++){
				if(max < result[i][j] && i!=j){
					max = result[i][j]
				}
			}
		}
		console.log('max = ' + max )
		io.emit('max', max)
	});
        socket.on('level', function(level){
          data = createNodesAndEdges(level)
          io.emit('data', data)
        })
        socket.on('nodenames', function(nodenames){
	  for(var i=0; i<nodenames.length; i++){
            nodes[Number(nodenames[i].id)] = nodenames[i].name
            io.emit('data', data)
          }
	})
});

http.listen(3000, function(){
	console.log('listening on *:3000');
});

function createNodesAndEdges(min){
  var data = {};
  data.edges = [];
  data.nodes = [];

  for(var i=0; i<support_table.length; i++){
    if(!nodes[i]){
      nodes[i] = String(i)
    }
  }

  for(var i=0; i<support_table.length; i++){
    for(var j=i; j<support_table[i].length; j++){
      if(i==j){
        continue
      }
      if(support_table[i][j] >= min){
        var edge = {src:i, dst:j, support:support_table[i][j] }
          data.edges.push(edge)
          isAdded = true
      }
    }
  }
  
  for(var i=0; i<support_table.length; i++){
    for(var j=0; j<support_table[i].length; j++){
      if(support_table[i][j] >= min && j!=i){
        var node = {id: i, name: String(i)}
        data.nodes.push(node)
        break
      }
    }
  }          
  console.log(data.nodes)
  return data
}

function analyzeAssociate(file){
	var lineString = file.toString().split("\n");
	var token = lineString[0].split(",");
	
	var column_count = token.length;
	
	var label = {};
	
	for(var i=0; i < token.length; i++){
		if(token[i] == "\"id\""){
			label.id = i;
		}
		
		if(token[i] == "\"beacon_id\""){
			label.beacon_id = i;
		}
		
		if(token[i] == "\"group_account_id\""){
			label.account_id = i;
		}
	}
	
	console.log("id = " + label.id + " beacon_id = " + label.beacon_id + " group_account_id = " + label.account_id);
	
	var records = [];
	
	for(var i=1; i< lineString.length; i++){
		token = lineString[i].split(",");
		var record = {};
		if(token.length == column_count){
			record.id = Number(token[label.id]);
			record.beacon_id = Number(token[label.beacon_id]);
			record.account_id = Number(token[label.account_id]);
		
			records.push(record);
		}
	}
	
	
	var cnt=0;
	for(var i=0; i<records.length; i++)
	{
		if(beacon_count < records[i].beacon_id){
			//console.log(i+") beacon_count "+beacon_count+"=>"+records[i].beacon_id);
			beacon_count = records[i].beacon_id;
		}
		
		if(transaction_count < records[i].account_id){
			//console.log(i+") transaction_count "+transaction_count+"=>"+records[i].account_id);
			transaction_count = records[i].account_id;
		}
		cnt++;
	}
	transaction_count++;
	beacon_count++;
	
	console.log("transaction_count:"+transaction_count+" beacon_count:"+beacon_count);

	transaction_table.length = transaction_count;
	for(var i=0; i<transaction_count; i++){
		transaction_table[i] = [];
		transaction_table[i].length = beacon_count;
		for(var j=0; j<beacon_count; j++){
			transaction_table[i][j] = 0;
		}
	}
	
	support_table.length = beacon_count;
	for(var i=0; i<beacon_count; i++){
		support_table[i] = [];
		support_table[i].length = beacon_count;
		for(var j=0; j<beacon_count; j++){
			support_table[i][j] = 0;
		}
	}
	
	for(var i=0; i<records.length; i++){
		
		transaction_table[records[i].account_id][records[i].beacon_id]++;
		
		if(transaction_table[records[i].account_id][records[i].beacon_id] > 1){
			transaction_table[records[i].account_id][records[i].beacon_id]=1;
		}
	}
	
	for(var i=0; i<beacon_count; i++){
		for(var j=0; j<beacon_count; j++){
				analyzeSupport(i,j);
		}
	}
	
	return support_table;
}

function analyzeSupport(x, y){
	for(var i=0; i<transaction_count; i++){
		if(transaction_table[i][x] > 0 && transaction_table[i][y] > 0){
			support_table[x][y]++;
		}
	}
}
